﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScript : MonoBehaviour {
    public Mob Opponent;
    public float CombatRange;
    public int Damage;
    private Animator anim;
    private float impactTime = 0.4f;
    private bool alreadyAttack = false;
    private bool alreadyAnimatingAttack = false;
    public int Health = 100;
    private StatsUi statsUi;

    void Start() {
        anim = GetComponent<PlayerMouvement>().Anim;
    }

    void Update() {
        if (Opponent != null) {
            bool isAttackAnimation = anim.GetCurrentAnimatorStateInfo(0).IsName("Attack");
            bool isIdleAnimation = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle");

            if (isIdleAnimation && !alreadyAnimatingAttack) {
                alreadyAttack = false;
                if (Vector3.Distance(transform.position, Opponent.transform.position) < CombatRange) {
                    if (!GetComponent<PlayerMouvement>().IsMoving) {
                        transform.LookAt(Opponent.transform);
                    }
                    anim.SetTrigger("AttackTrigger");
                    alreadyAnimatingAttack = true;
                }
            }

            if (isAttackAnimation && !alreadyAttack && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > impactTime) {
                if (!GetComponent<PlayerMouvement>().IsMoving) {
                    transform.LookAt(Opponent.transform);
                }
                Attack();
                alreadyAttack = true;
                alreadyAnimatingAttack = false;
            }
        }
    }

    private void Attack() {
        Opponent.TakeHit(Damage);
    }

    public void CancelAttack() {
        //   alreadyAttack = false;
        //  isAttackAnimation = false;
        alreadyAnimatingAttack = false;
    }

    public void ApplyDamage(int damage)
    {
        Health -= damage;

    }

    /*      if (Opponent != null) {
          //if we have an opponent selected
          isAttackAnimation = anim.GetCurrentAnimatorStateInfo(0).IsName("Attack");
              //check the current animation being played
          bool isIdleAnimation = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle");

          if (isIdleAnimation && !alreadyAnimatingAttack) {
              // if not on attack animation, alreadyAnimatingAttack has been added because isAttackAnimation seems to not be refresh correctly -> trigger fired multiple times
              if (Vector3.Distance(transform.position, Opponent.transform.position) < CombatRange) {
                  // if we are at range of the opponent
                  if (!GetComponent<PlayerMouvement>().IsMoving) // if we are not moving
                  {
                      transform.LookAt(Opponent.transform); //look at the opponent
                  }

                  //anim.SetBool("isWalking", false);
                  anim.SetTrigger("AttackTrigger"); //fire the attack animation
                  alreadyAttack = false;
                  alreadyAnimatingAttack = true;
              }
          }

          if (isAttackAnimation && !alreadyAttack && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > impactTime) {
              //check if we have not already damaged the ennemy in this animation and if we are at the impact time of the animation
              if (!GetComponent<PlayerMouvement>().IsMoving) {
                  transform.LookAt(Opponent.transform); // if not moving, look at the opponent
              }
              Attack(); //actually damage the opponent
              alreadyAttack = true;
              alreadyAnimatingAttack = false;
          }
      }
      else {
          alreadyAttack = false; //if no opponent, we have not already attacked, (useless ?)
          alreadyAnimatingAttack = false;
      }*/
}