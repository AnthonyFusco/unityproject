﻿using UnityEngine;
using UnityEngine.EventSystems;

public class StatsUi : MonoBehaviour {
    private CombatScript combatScript;
    private int health;

    private void Start() {
        combatScript = transform.GetComponentInParent<CombatScript>();
    }

    private void Update() {
        health = combatScript.Health;

        if (health <= 0)
        {
            Dead();
        }
    }


    public void Dead() {
        Debug.Log("YOU'RE DEAD");
    }

    public void OnGUI() {
        GUI.Box(new Rect(30, 20, 90, 20), "health :" + health);
    }
}