﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : MonoBehaviour {
    public int Health = 100;
    public CombatScript Player;

    public void TakeHit(int damage) {
        Health -= damage;
        Debug.Log(gameObject + " hit : " + Health);
        if (Health <= 0) {
            Player.Opponent = null;
            Destroy(gameObject);
        }
    }

    public void SetRendererColor(Color color) {
        //GetComponent<Renderer>().material.color = color;
        transform.Find("WK_HeavyIntantry").GetComponent<Renderer>().material.color = color;
    }
    
}
