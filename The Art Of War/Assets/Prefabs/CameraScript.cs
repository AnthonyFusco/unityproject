﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden

using UnityEngine;
using System.Collections;

// Place the script in the Camera-Control group in the component menu
[AddComponentMenu("Camera-Control/Smooth Follow CSharp")]

public class CameraScript : MonoBehaviour
{
    
    public Transform target;
    public float distance = 10.0f;
    public float height = 5.0f;
    public float heightDamping = 2.0f;

    void LateUpdate()
    {
        if (!target)
            return;

        float wantedHeight = target.position.y + height;
        float currentHeight = transform.position.y;
        
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
        
        transform.position = target.position;
        transform.position -= Vector3.forward * distance;
        
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
        
        transform.LookAt(target);
    }
}