﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerMouvement : MonoBehaviour
{

    private Vector3 position;
    public NavMeshAgent NavMeshAgent;
    public Animator Anim;
    public float Speed;
    public ParticleSystem Dust;
    private Mob lastEnnemySelected;
    public bool IsMoving = false;
    private const float navMeshSampleDistance = 4f;
    public int MinimumDistance;

    private void Start() {
        position = transform.position;
    }


    private void Update() {
        MoveToPosition();
    }

  /* 
    private void LocatePosition()
    {
       RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000, floorMask)) {
            Debug.Log(hit.collider.name);
            
            position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            Dust.Clear();
        }
    }
    */

    public void OnGroundClick(BaseEventData data) {
        PointerEventData pData = (PointerEventData) data;
        //position = pData.pointerCurrentRaycast.worldPosition;

        NavMeshHit hit;
        if (NavMesh.SamplePosition(pData.pointerCurrentRaycast.worldPosition, out hit, navMeshSampleDistance, NavMesh.AllAreas))
            position = hit.position;

        Dust.Clear();
        GetComponent<CombatScript>().CancelAttack();
    }

    public void OnMobClick(Mob mob)
    {
        GetComponent<CombatScript>().Opponent = mob;
        mob.Player = GetComponent<CombatScript>();

        if (lastEnnemySelected != null)
        {
            lastEnnemySelected.SetRendererColor(Color.white);
        }
        mob.SetRendererColor(Color.red);

        lastEnnemySelected = mob;

        position = mob.transform.position;
       // GetComponent<CombatScript>().CancelAttack();
    }

    private void MoveToPosition() {
        position.y = 0;
        if (Vector3.Distance(transform.position, position) > MinimumDistance) {
            IsMoving = true;
            var newRotation = Quaternion.LookRotation(position - transform.position, Vector3.forward);
            
            newRotation.x = 0f;
            newRotation.z = 0f;

            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 10);
            //Controller.SimpleMove(transform.forward * Speed);
            NavMeshAgent.SetDestination(position);
            NavMeshAgent.Resume();

            Anim.SetBool("isWalking", true);

            Dust.transform.position = position;
            if (!Dust.isPlaying)
            {
                Dust.Play();
            }

        }
        else {
            if (Dust.isPlaying)
                Dust.Stop();

            IsMoving = false;
            Dust.Clear();
            Anim.SetBool("isWalking", false);
        }
    }

}